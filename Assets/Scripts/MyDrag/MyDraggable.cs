﻿using UnityEngine;
using UnityEngine.UI;
using System.Collections;
using UnityEngine.EventSystems;
public class MyDraggable : MonoBehaviour,IBeginDragHandler,IDragHandler,IEndDragHandler
{
    
    static MyDraggable _instance;
    public static MyDraggable instance{
        get{
            if (_instance == null){
                _instance = FindObjectOfType(typeof(MyDraggable)) as MyDraggable;
                if (_instance == null){
                    GameObject go = new GameObject("MyDraggable");
                    _instance = go.AddComponent<MyDraggable>();
                }
            }
            return _instance;
        }
    }

    public Transform parentToReturnTo=null;

    private GameObject thisObject;


    

    
    private void Update() {
        
    
        
    }
    

    public void OnBeginDrag(PointerEventData eventData)
    {
        
        Debug.Log(1);
        //parentToReturnTo=this.transform.parent;
        //parentToReturnTo=GameManagerMan.instance.Canvas.transform;
        this.transform.SetParent(GameManagerMan.instance.Canvas.transform);
        this.GetComponent<CanvasGroup>().blocksRaycasts=false;
        
        

    }

    public void OnDrag(PointerEventData eventData)
    {
        
        Debug.Log(2);
        this.transform.position=eventData.position;
        
    }

    public void OnEndDrag(PointerEventData eventData)
    {
        
        Debug.Log(3);
        this.transform.SetParent(parentToReturnTo);
        this.GetComponent<CanvasGroup>().blocksRaycasts=true;
        
       
    }


    
}

