﻿using System.Collections;
using System.Collections.Generic;
using UnityEngine;
using UnityEngine.EventSystems;

public class MyDropZone : MonoBehaviour,IDropHandler,IPointerEnterHandler,IPointerExitHandler
{
    public GameObject thisZoom;
    public void OnPointerEnter(PointerEventData eventData)
    {
        
    }

    public void OnPointerExit(PointerEventData eventData)
    {
        
    }

    public void OnDrop(PointerEventData eventData)
    {
        
            MyDraggable d=eventData.pointerDrag.GetComponent<MyDraggable>();
            if(d!=null)
            {
                d.parentToReturnTo=this.transform;
            }
       
    }
    
}