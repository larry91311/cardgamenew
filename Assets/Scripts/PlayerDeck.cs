﻿using System.Collections;
using System.Collections.Generic;
using UnityEngine;

public class PlayerDeck : MonoBehaviour
{
    public List<Card> deck=new List<Card>();
    public List<Card> container=new List<Card>();   //隨機Shuffle(Swap)時塞一個當時要換位置的原牌
    public int x;      //隨機數字，thisId載體
    public static int deckSize;

    


    [Header("Deck")]
    public GameObject cardInDeck1;
    public GameObject cardInDeck2;
    public GameObject cardInDeck3;
    public GameObject cardInDeck4;


    [Header("HandZoneANDDRAW")]
    public static List<Card> staticDeck=new List<Card>();
    public GameObject Hand;
    public GameObject CardToHandNew;
    private void Awake() {
        
    }

    private void Start() {
        
        x=0;             
        deckSize=40;
        for(int i=0;i<40;i++)   //create deck
        {
            x=Random.Range(1,6);
            deck[i]=CardDataBase.cardList[x];
        }

        StartCoroutine(StartGame());
    }






    private void Update() {
        staticDeck=deck;

        if(deckSize<30)
        {
            cardInDeck1.SetActive(false);
        }
        if(deckSize<20)
        {
            cardInDeck2.SetActive(false);
        }
        if(deckSize<2)
        {
            cardInDeck3.SetActive(false);
        }
        if(deckSize<1)
        {
            cardInDeck4.SetActive(false);
        }

        if(ThisCard.drawX>0)
        {
            StartCoroutine(Draw(ThisCard.drawX));
            ThisCard.drawX=0;
        }
        if(TurnSystem.startTurn==true)
        {
            StartCoroutine(Draw(1));
            TurnSystem.startTurn=false;
        }
    }


    public void Shuffle()
    {
        for(int i=0;i<40;i++)
        {
            container[0]=deck[i];    //原進container
            int randomIndex=Random.Range(i,deckSize);
            deck[i]=deck[randomIndex];     //原=RAN
            deck[randomIndex]=container[0];    //RAN=container內原
        }
    }


    IEnumerator StartGame()
    {
        for(int i=0;i<=4;i++)
        {
            yield return new WaitForSeconds(1);
            GameObject inCard=Instantiate(CardToHandNew,transform.position,transform.rotation);
            
            
        }

    }

    IEnumerator Draw(int x)
    {
        for(int i=0;i<x;i++)
        {
            yield return new WaitForSeconds(1);
            GameObject inCard=Instantiate(CardToHandNew,transform.position,transform.rotation);
            
            
        }

    }





}
