﻿using System.Collections;
using System.Collections.Generic;
using UnityEngine;
using UnityEngine.UI;
public class CardDataBase : MonoBehaviour
{
    public static List<Card> cardList=new List<Card>();



    private void Awake() {
        cardList.Add(new Card(0,"None",0,0,"None",Resources.Load<Sprite>("None"),0,0,0));
        cardList.Add(new Card(1,"Elf",2,1000,"Draw 2 Cards",Resources.Load<Sprite>("Elf"),2,0,0));
        cardList.Add(new Card(2,"Dwarf",3,3000,"Add 1 MaxMana",Resources.Load<Sprite>("Dwarf"),0,1,0));
        cardList.Add(new Card(3,"Human",5,6000,"Add 3 MaxManas",Resources.Load<Sprite>("Human"),0,3,0));
        cardList.Add(new Card(4,"Demon",1,1000,"Draw 1 Card",Resources.Load<Sprite>("Demon"),1,0,0));
        cardList.Add(new Card(5,"Priest",1,1000,"Return 1 Card From Graveyard",Resources.Load<Sprite>("Priest"),0,0,1));

    }
}
