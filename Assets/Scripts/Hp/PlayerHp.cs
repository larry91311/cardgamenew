﻿using System.Collections;
using System.Collections.Generic;
using UnityEngine;
using UnityEngine.UI;

public class PlayerHp : MonoBehaviour
{
    public static float maxHp;
    public static float staticHp;
    public float hp;
    public Image Health;
    public Text hpText;



    private void Start() {
        maxHp=25000;
        staticHp=15000;
    }


    private void Update() {
        hp=staticHp;
        Health.fillAmount=hp/maxHp;

        if(hp>=maxHp)
        {
            hp=maxHp;
        }

        hpText.text=hp+"HP";
        
    }
}
