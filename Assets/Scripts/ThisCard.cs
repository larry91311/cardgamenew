﻿using System.Collections;
using System.Collections.Generic;
using UnityEngine;
using UnityEngine.UI;
using UnityEngine.EventSystems;

public class ThisCard : MonoBehaviour
{
    public List<Card> thisCard=new List<Card>();

    public int thisId;
    public int id;
    public string cardName;
    public int cost;
    public int power;
    public string cardDescription;


    public Text nameText;
    public Text costText;
    public Text powerText;
    public Text descriptionText;


    
    public Sprite thisSprite;   //從DATABASE取得的載體
    public Image thatImage;   //UI

    //[Header("CardBack")]
    public bool cardBack;
    public static bool staticCardBack;


    //[Header("HandZoneAndDraw")]
    private GameObject Hand;
    public int numberOfCardsInDeck;


    //[Header("SummoningAndDragDrop")]
    public bool canBeSummon;
    public bool summoned;
    private GameObject battleZone;

    private bool befalseOnce=false;

    //[Header("CardEffect")]
    public static int drawX;
    public int drawXcards;
    public int addXmaxMana;

    //[Header("Attack&TargetSystem")]
    public GameObject attackBorder;
    public GameObject Target;
    public GameObject Enemy;

    public bool summoningSickness;
    public bool cantAttack;
    public bool canAttack;
    
    public static bool staticTargeting;
    public static bool staticTargetingEnemy;

    public bool targeting;
    public bool targetingEnemy;
    public bool onlyThisCardAttack;

    //[Header("Summoning Border")]
    public GameObject summonBorder;


    //[Header("Destroy Card")]
    public bool canBeDestroyed; //決定是否放進墓地
    public GameObject Graveyard;
    public bool beInGraveyard;  //是否在

    //[Header("ReturnCardFromGraveyard")]
    public int hurted=0;
    public int actualpower;
    public int returnXcards;
    public bool useReturn;
    public static bool UcanReturn;



    

    private void Start() {
        thisCard[0]=CardDataBase.cardList[thisId];       //取得第0個cardList(牌型)的資料，根據thisID
        cardBack=false;


        numberOfCardsInDeck=PlayerDeck.deckSize;
        canBeSummon=false;
        summoned=false;

        drawX=0;

        canAttack=false;
        summoningSickness=true;
        Enemy=GameManagerMan.instance.EnemyHP;     //Attack初始狀態設定
        targeting=false;
        targetingEnemy=false;

        actualpower=thisCard[0].power;
        // UcanReturn=false;
        // beInGraveyard=false;
        // useReturn=false;



        

    } 


    private void Update() {
        Hand=GameObject.Find("Hand");
        if(this.gameObject.transform.parent==Hand.transform.parent)    //手上就顯示，蓋牌(卡背)=false
        {
            cardBack=false;
        }


        id=thisCard[0].id;
        cardName=thisCard[0].cardName;
        cost=thisCard[0].cost;
        power=thisCard[0].power;
        cardDescription=thisCard[0].cardDescription;          //取得第0個cardList(牌型)的資料，根據thisID
        thisSprite=thisCard[0].thisImage;
        

        drawXcards=thisCard[0].drawXcards;
        addXmaxMana=thisCard[0].addXmaxMana;         //效果數值取得
        returnXcards=thisCard[0].returnXCards;


        nameText.text=""+cardName;
        costText.text=""+cost;
        powerText.text=""+power;
        descriptionText.text=""+cardDescription;          //賦予UI取得後的數據
        thatImage.sprite=thisSprite;

        staticCardBack=cardBack;     //Inspector測試，傳回值

        
        if(this.gameObject.tag=="Clone")    //一創造，進Update判斷，是抽出來的牌(tag==Clone)就=staticDeck最尾巴的牌，然後牌庫內牌跟decksize都減1(還未remove deck牌)，然後改tag、取消蓋牌
        {
            thisCard[0]=PlayerDeck.staticDeck[numberOfCardsInDeck-1];
            numberOfCardsInDeck-=1;
            PlayerDeck.deckSize-=1;
            cardBack=false;
            this.gameObject.tag="Untagged";
        }

        if(TurnSystem.currentMana>=cost&&summoned==false&&beInGraveyard==false)
        {
            canBeSummon=true;
        }
        else
        {
            canBeSummon=false;
        }

        if(canBeSummon==true||(UcanReturn==true&&beInGraveyard==true))
        {
            
            this.gameObject.GetComponent<MyDraggable>().enabled=true;          //出大事囉，不能拉的BUG位置所在(已修:Draggable加空Update)
        }
        else if(canBeSummon==false)
        {
            
            
           this.gameObject.GetComponent<MyDraggable>().enabled=false;         //出大事囉，不能拉的BUG位置所在
        }

        battleZone=GameObject.Find("Zone");

        if(summoned==false&&this.transform.parent==battleZone.transform)
        {
            Summon();
        }

        if(canAttack==true&&beInGraveyard==false)    //attack不能在墓地
        {
            attackBorder.SetActive(true);
        }
        else
        {
            attackBorder.SetActive(false);
        }

        if(TurnSystem.isYourTurn==false&&summoned==true)   //已經召喚過+敵方回合，睡一回合後可以攻擊
        {
            UcanReturn=false;
            summoningSickness=false;     //睡眠  結束
            cantAttack=false;             //可以攻擊
        }

        if(TurnSystem.isYourTurn==true&&summoningSickness==false&&cantAttack==false)  //己方回合+已經睡過+可以攻擊
        {
            canAttack=true;   //允許攻擊
        }
        else
        {
            canAttack=false;  //不允許攻擊
        }

        targeting=staticTargeting;
        targetingEnemy=staticTargetingEnemy;

        if(targetingEnemy==true)
        {
            Target=Enemy;   //設定敵人
        }
        else
        {
            Target=null;
        }

        if(targeting==true&&targetingEnemy==true&&onlyThisCardAttack==true)
        {
            Attack();
        }


        if(canBeSummon==true)
        {
            summonBorder.SetActive(true);         //召喚框顯示
        }
        else
        {
            summonBorder.SetActive(false);
        }

        if(actualpower<=0)
        {
            Destroy();                //外部測試方便(生命值<0)移入墓地
        }

        if(returnXcards>0&&summoned==true&&useReturn==false)
        {
            Return(returnXcards);
            useReturn=true;
        }



        if(TurnSystem.currentMana==8&&summoned==true)    //Test Destroy
        {
            Destroy();
        }


        if(TurnSystem.isYourTurn==false)
        {
            UcanReturn=false;
        }

        

        


        
    }



    public void Summon()
    {
        TurnSystem.currentMana-=cost;   //消耗費用
        summoned=true;                  //已經召喚
        MaxMana(addXmaxMana);           //效果:加Mana
        drawX=drawXcards;               //效果:抽卡
    }



    public void MaxMana(int x)      //外部輸入、加水晶
    {
        TurnSystem.maxMana+=x;
    }

    public void Attack()
    {
        if(canAttack==true&&summoned==true)
        {
            if(Target!=null)
            {
                if(Target==Enemy)
                {
                    OpponentHp.staticHp-=power;
                    targeting=false;
                }

                if(Target.name=="CardToHand(Clone)")
                {
                    canAttack=true;
                }
            }
        }
    }

    public void UntargetEnemy()
    {
        staticTargetingEnemy=false;
    }

    public void TargetEnemy()
    {
        staticTargetingEnemy=true;
    }


    public void StartAttack()
    {
        staticTargeting=true;
    }

    public void StopAttack()
    {
        staticTargeting=false;
    }
    public void OneCardAttack()
    {
        onlyThisCardAttack=true;
    }
    public void OneCardAttackStop()
    {
        onlyThisCardAttack=false;
    }



    public void Destroy()      //放進墓地，非真實Destroy
    {
        Graveyard=GameManagerMan.instance.Graveyard;
        canBeDestroyed=true;
        if(canBeDestroyed==true)
        {
            this.transform.SetParent(Graveyard.transform);
            canBeDestroyed=false;
            summoned=false;
            beInGraveyard=true;
            hurted=0;  //傷害敵人量
        }
    }

    public void ReturnCard()
    {
        UcanReturn=true;
    }
    public void Return(int x)
    {
        for(int i=0;i<x;i++)
        {
            ReturnCard();
        }
    }

    public void ReturnThis()
    {
        if(beInGraveyard==true&&UcanReturn==true)
        {
            this.transform.SetParent(Hand.transform);
            UcanReturn=false;
            beInGraveyard=false;
            summoningSickness=true;
        }
    }



}
