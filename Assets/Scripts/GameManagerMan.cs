﻿using System.Collections;
using System.Collections.Generic;
using UnityEngine;

public class GameManagerMan : MonoBehaviour
{
   
    static GameManagerMan _instance;
    public static GameManagerMan instance{
        get{
            if (_instance == null){
                _instance = FindObjectOfType(typeof(GameManagerMan)) as GameManagerMan;
                if (_instance == null){
                    GameObject go = new GameObject("GameManagerMan");
                    _instance = go.AddComponent<GameManagerMan>();
                }
            }
            return _instance;
        }
    }



    public GameObject Hand;
    public GameObject Zone;
    
    public GameObject Canvas;

    public GameObject EnemyHP;  //Enemy頭像
    public GameObject Graveyard;

    
    
    // Start is called before the first frame update
    void Start()
    {
        
    }

    // Update is called once per frame
    void Update()
    {
        
    }
}
