﻿using System.Collections;
using System.Collections.Generic;
using UnityEngine;
using UnityEngine.UI;

[System.Serializable]
public class Card 
{
    public int id;
    public string cardName;
    public int cost;
    public int power;
    public string cardDescription;
    public Sprite thisImage;


    public int drawXcards;     //抽卡效果載體
    public int addXmaxMana;    //加水晶效果載體
    public int returnXCards;   //復活效果載體





    public Card()
    {

    }


    public Card(int Id,string CardName,int Cost,int Power,string CardDescription,Sprite ThisImage,int DrawXcards,int AddXmaxMana,int ReturnXCards)
    {
        id=Id;
        cardName=CardName;
        cost=Cost;
        power=Power;
        cardDescription=CardDescription;
        thisImage=ThisImage;
        drawXcards=DrawXcards;
        addXmaxMana=AddXmaxMana;
        returnXCards=ReturnXCards;
    }
    
}
