﻿using System.Collections;
using System.Collections.Generic;
using UnityEngine;
using UnityEngine.UI;
using UnityEngine.EventSystems;

public class ThisCardEmpty : MonoBehaviour
{
    public List<Card> thisCard=new List<Card>();

    public int thisId;
    public int id;
    public string cardName;
    public int cost;
    public int power;
    public string cardDescription;


    public Text nameText;
    public Text costText;
    public Text powerText;
    public Text descriptionText;


    
    public Sprite thisSprite;   //從DATABASE取得的載體
    public Image thatImage;   //UI

    [Header("CardBack")]
    public bool cardBack;
    public static bool staticCardBack;


    [Header("HandZoneAndDraw")]
    public GameObject Hand;
    public int numberOfCardsInDeck;


    [Header("SummoningAndDragDrop")]
    public bool canBeSummon;
    public bool summoned;
    public GameObject battleZone;

    private bool befalseOnce=false;



    public GameObject cardBody;


    

    private void Start() {
        thisCard[0]=CardDataBase.cardList[thisId];       //取得第0個cardList(牌型)的資料，根據thisID
        cardBack=true;


        numberOfCardsInDeck=PlayerDeck.deckSize;
        canBeSummon=false;
        summoned=false;
    } 


    private void Update() {
        Hand=GameManagerMan.instance.Hand;
        if(cardBody.transform.parent==Hand.transform.parent)    //手上就顯示，蓋牌(卡背)=false
        {
            cardBack=false;
        }


        id=thisCard[0].id;
        cardName=thisCard[0].cardName;
        cost=thisCard[0].cost;
        power=thisCard[0].power;
        cardDescription=thisCard[0].cardDescription;          //取得第0個cardList(牌型)的資料，根據thisID
        thisSprite=thisCard[0].thisImage;


        nameText.text=""+cardName;
        costText.text=""+cost;
        powerText.text=""+power;
        descriptionText.text=""+cardDescription;          //賦予UI取得後的數據
        thatImage.sprite=thisSprite;

        staticCardBack=cardBack;     //Inspector測試，傳回值


        if(cardBody.tag=="Clone")    //一創造，進Update判斷，是抽出來的牌(tag==Clone)就=staticDeck最尾巴的牌，然後牌庫內牌跟decksize都減1(還未remove deck牌)，然後改tag、取消蓋牌
        {
            thisCard[0]=PlayerDeck.staticDeck[numberOfCardsInDeck-1];
            numberOfCardsInDeck-=1;
            PlayerDeck.deckSize-=1;
            cardBack=false;
            cardBody.tag="Untagged";
        }

        if(TurnSystem.currentMana>=cost&&summoned==false)
        {
            canBeSummon=true;
        }
        else
        {
            canBeSummon=false;
        }

        if(canBeSummon==true)
        {
            cardBody.gameObject.GetComponent<MyDraggable>().enabled=true;
        }
        else if(canBeSummon==false)
        {
           cardBody.gameObject.GetComponent<MyDraggable>().enabled=false;
        }

        battleZone=GameManagerMan.instance.Zone;

        if(summoned==false&&this.transform.parent==battleZone.transform)
        {
            Summon();
        }


        
    }



    public void Summon()
    {
        TurnSystem.currentMana-=cost;
        summoned=true;
    }
}
