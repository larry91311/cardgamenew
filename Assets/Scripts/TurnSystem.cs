﻿using System.Collections;
using System.Collections.Generic;
using UnityEngine;
using UnityEngine.UI;

public class TurnSystem : MonoBehaviour
{
    public static bool isYourTurn;
    public int yourTurn;
    public int yourOpponentTurn;
    public Text turnText;


    public static int maxMana;
    public static int currentMana;
    public Text manaText;


    public static bool startTurn;

    private void Start() {
        isYourTurn=true;
        yourTurn=1;
        yourOpponentTurn=0;

        maxMana=1;
        currentMana=1;

        startTurn=false;
    }


    private void Update() {
        if(isYourTurn==true)
        {
            turnText.text="Your Turn";

        }
        else
        {
            turnText.text="Opponent Turn";
        }

        manaText.text=currentMana+"/"+maxMana;
    }


    public void EndYourTurn()
    {
        isYourTurn=false;
        yourOpponentTurn+=1;
    }

    public void EndYourOpponentTurn()
    {
        isYourTurn=true;
        yourTurn+=1;
        maxMana+=1;
        currentMana=maxMana;

        startTurn=true;   //我的回合開始->抽牌
    }
}
